# Pattern Observer 
El patrón Observer es un patrón de diseño, de comportamiento, que permite definir un mecanismo de suscripción para notificar a varios objetos sobre cualquier evento que le suceda al objeto que se está observando.

## Problema
Imagine que tiene dos tipos de objetos: un objeto Cliente y un objeto Farmacia. El cliente está muy interesado en un nuevo Medicamento que en breve estará disponible en la Farmacia.

El cliente puede ir a la farmacia todos los días para preguntar si ya llego el medicamento. Pero, mientras el medicamento está en camino, la mayoría de estas visitas a la farmacia serán en vano.

#### Visita vs envío de spam
Por otro lado, la farmacia podría enviar correos (lo cual se podría considerar spam) a todos los clientes cada vez que un nuevo medicamento llega. Esto ahorraría a los clientes las visitas a la farmacia, pero, al mismo tiempo, molestaría a otros clientes que no están interesados en estos medicamentos nuevos.

Parece que nos encontramos ante un conflicto. O el cliente pierde tiempo comprobando la disponibilidad del medicamento, o bien la farmacia desperdicia recursos notificando(molestando) a los clientes equivocados.

## Solución
El objeto que tiene un estado interesante suele denominarse Observable, pero, como también va a notificar a otros objetos los cambios en su estado, tambien se le puede llamar Notificador o Publicador. 
El resto de los objetos que quieren conocer los cambios en el estado del Observable, se denominan Observadores.

El patrón Observer sugiere añadir un mecanismo de suscripción a la clase observable para que los objetos individuales(observadores) puedan suscribirse o cancelar su suscripción a los cambios de estado del observable. 
Este mecanismo consiste en: 

1. una colección para almacenar las referencias a objetos observadores y 
2. un método público que permita añadir observadores
3. un método público que permita eliminar observadores.

Cuando ocurre un cambio de estado al observable, recorre sus observadores y llama al método de notificación específico de cada objetos. 

Las aplicaciones reales pueden tener decenas de clases observadoras diferentes interesadas en seguir los eventos de la misma clase observable. No es buena idea acoplar la clase observable a todas esas clases. 
Además, puede que no conozcas algunas de ellas de antemano si se supone que otras personas pueden utilizar tu clase observable.

Por eso es fundamental que todos los observadores implementen la misma interfaz y que el observable únicamente se comunique con ellos a través de esa interfaz. Esta interfaz debe declarar el método de notificación junto con un grupo de parámetros que el observable puede utilizar para pasar cierta información contextual con la notificación.

Si tu aplicación tiene varios tipos diferentes de observables y quieres hacer a tus observadores compatibles con todos ellos, puedes ir más allá y hacer que todos los observables sigan la misma interfaz. Esta interfaz sólo tendrá que describir algunos métodos de suscripción. La interfaz permitirá a los observadores observar los estados de los observable sin acoplarse a sus clases concretas.

## Estructura UML
![](model.svg)
