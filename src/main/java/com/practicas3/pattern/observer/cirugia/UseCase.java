package com.practicas3.pattern.observer.cirugia;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class UseCase {

	private Map<Long, Cirugia> cirugias = new HashMap<Long, Cirugia>();
	private Map<String, Pasiente> pasientes = new HashMap<String, Pasiente>();

	public void agregarPasiente(String nombre) {
		log.debug("Se agrega Pasiente con nombre {}.", nombre);
		this.pasientes.put(nombre, new Pasiente(nombre));
	}
	
	public void agregarCirugia(String nombrePasiente, LocalDate fecha, String descripcion) {
		log.debug("Se agrega una Cirugia para el Pasiente {}. Descripcion de la cirugia: {}.", nombrePasiente, descripcion);
		Pasiente pasiente = this.pasientes.get(nombrePasiente);
		Long id = Long.valueOf(this.cirugias.size() + 1);
		cirugias.put(id, new Cirugia(id, pasiente, fecha, descripcion));
	}
	
	public void agregarCirujano(long idCirugia, String nombre) {
		log.debug("Se agrega Cirujano con nombre {}. A la Cirugia con id: {}.", nombre, idCirugia);
		Cirugia cirugia = this.cirugias.get(idCirugia);
		cirugia.agregarCirugano(new Cirujano(nombre));
	}
	
	public void agregarFamiliar(long idCirugia, String nombre, String parentesco) {
		log.debug("Se agrega Familiar con nombre {} y parentesco {}. A la Cirugia con id: {}.", nombre, parentesco, idCirugia);
		Cirugia cirugia = this.cirugias.get(idCirugia);
		cirugia.agregarFamiliar(new Familiar(nombre, parentesco));
	}

	public void posponerCirugia(long idCirugia, int dias) {
		log.debug("Se pospone {} dias la Cirugia con id {}.", dias, idCirugia);
		Cirugia cirugia = this.cirugias.get(idCirugia);
		cirugia.posponer(dias);
	}
	
	public Cirugia getCirugia(long idCirugia) {
		return this.cirugias.get(idCirugia);
	}
}
