package com.practicas3.pattern.observer.cirugia;

public interface Observador {

	public void notificarme(String notificacion);
}
