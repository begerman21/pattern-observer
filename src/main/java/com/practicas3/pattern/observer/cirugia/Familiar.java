package com.practicas3.pattern.observer.cirugia;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

@Getter
@ToString
@Log4j2
@AllArgsConstructor
public class Familiar implements Observador {

	private String nombre;
	private String parentesco;

	@Override
	public void notificarme(String notificacion) {
		log.debug("Se notifica: {}", notificacion);
	}

}
