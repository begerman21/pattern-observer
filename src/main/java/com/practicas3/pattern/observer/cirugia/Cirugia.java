package com.practicas3.pattern.observer.cirugia;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

@Getter
@ToString
@Log4j2
public class Cirugia {

	private long id;
	private Pasiente pasiente;
	private LocalDate fecha;
	private String descripcion;

	private List<Observador> observadores = new ArrayList<>();

	public Cirugia(long id, Pasiente pasiente, LocalDate fecha, String descripcion) {
		super();
		this.id = id;
		this.pasiente = pasiente;
		this.fecha = fecha;
		this.descripcion = descripcion;
		this.suscribir(pasiente);
		this.notificar("Se programa la cirugia para la fecha " + this.fecha + ".");
	}

	public void agregarFamiliar(Familiar familiar) {
		this.suscribir(familiar);
		this.notificar(
				"Se agrego el familiar de contacto " + familiar.getNombre() + "(" + familiar.getParentesco() + ")");
		// logica de negocio
	}

	public void agregarCirugano(Cirujano cirujano) {
		this.suscribir(cirujano);
		this.notificar("Se agrego el cirujano " + cirujano.getNombre() + ".");
		// logica de negocio
	}

	public void posponer(int dias) {
		this.fecha = this.fecha.plusDays(dias);
		this.notificar("La cirugia se pospone para la fecha " + this.fecha + ".");
	}

	public void notificar(String notificacion) {
		for (Observador observador : observadores) {
			observador.notificarme(notificacion);
		}
	}

	public void suscribir(Observador observador) {
		this.observadores.add(observador);
	}

	public void desuscribir(Observador observador) {
		this.observadores.remove(observador);
	}

}
