package com.practicas3.pattern.observer.gui;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.event.MouseInputAdapter;

public class Boton extends JButton {

	private static final long serialVersionUID = 1L;
	private List<Observador> observadores = new ArrayList<Observador>();
	private String ayuda;

	public Boton(String text, String ayuda) {
		super(text);
		this.ayuda = ayuda;

		this.addMouseListener(new MouseInputAdapter() {
			public void mouseEntered(MouseEvent e) {
				notificar(ayuda);
			}

			public void mouseExited(MouseEvent e) {
				notificar("...");
			}
		});
	}

	public void suscribir(Observador observador) {
		this.observadores.add(observador);
	}

	public void desuscribir(Observador observador) {
		this.observadores.remove(observador);
	}

	public void notificar(String notificacion) {
		for (Observador o : this.observadores) {
			o.notificarme(notificacion);
		}
	}

}
