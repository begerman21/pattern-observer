package com.practicas3.pattern.observer.gui;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class LeyendaPanel extends JPanel implements Observador {

	private static final long serialVersionUID = 1L;

	private JLabel lblLeyenda;

	public LeyendaPanel() {
		super();

		this.lblLeyenda = new JLabel("...");
		this.lblLeyenda.setForeground(Color.YELLOW);
		this.setBackground(Color.GRAY);
		this.add(lblLeyenda);
	}

	@Override
	public void notificarme(String notificacion) {
		this.lblLeyenda.setText(notificacion);
	}

}
