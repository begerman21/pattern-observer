package com.practicas3.pattern.observer.gui;

public interface Observador {

	public void notificarme(String notificacion);
}
