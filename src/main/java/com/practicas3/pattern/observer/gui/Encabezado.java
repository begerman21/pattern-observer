package com.practicas3.pattern.observer.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Encabezado extends JPanel {

	private static final long serialVersionUID = 1L;

	private JLabel titulo;
	private JLabel subTitulo;
	private JLabel mensaje;
	
	public Encabezado(String titulo, String subTitulo) {
		super();
		this.setBackground(Color.WHITE);
		this.setLayout(new GridLayout(3,1));
		
		this.titulo = new JLabel(titulo);
		this.titulo.setHorizontalAlignment(JLabel.CENTER);
		this.titulo.setForeground(Color.BLUE);
		this.titulo.setFont(new Font("aakar", Font.BOLD, 26));
		
		this.subTitulo = new JLabel(subTitulo);
		this.subTitulo.setHorizontalAlignment(JLabel.CENTER);
		this.subTitulo.setForeground(Color.BLUE);

		this.mensaje = new JLabel("Hola ventana");
		this.mensaje.setForeground(Color.MAGENTA);
		
		this.add(this.titulo);
		this.add(this.subTitulo);
		this.add(this.mensaje);
	}

	
}
