package com.practicas3.pattern.observer.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;

public class Botonera extends JPanel {

	private static final long serialVersionUID = 1L;
	private Boton guardar;
	private Boton cancelar;
	private Boton modificar;
	private Boton eliminar;
	private Boton salir;

	public Botonera() {
		super(new BorderLayout());

		LeyendaPanel leyenda = new LeyendaPanel();
		this.add(leyenda, BorderLayout.SOUTH);

		JPanel botones = new JPanel();
		this.guardar = new Boton("Guardar", "Click para guardar los datos del formulario...");
		this.guardar.suscribir(leyenda);
		this.guardar.setEnabled(false);
		this.cancelar = new Boton("Cancelar", "Click para cancelar la operacion y no guardar los datos del formulario...");
		this.cancelar.suscribir(leyenda);
		this.cancelar.setEnabled(false);
		this.modificar = new Boton("Modificar", "Click para habilita el formulario para realizar una modificacion de datos...");
		this.modificar.suscribir(leyenda);
		this.eliminar = new Boton("Eliminar", "Click para eliminar...");
		this.eliminar.suscribir(leyenda);
		this.salir = new Boton("Salir","Click para salir de la ventana...");
		this.salir.suscribir(leyenda);

		botones.add(this.guardar);
		botones.add(this.cancelar);
		botones.add(this.modificar);
		botones.add(this.eliminar);
		botones.add(this.salir);

		this.add(botones, BorderLayout.CENTER);

	}

}
