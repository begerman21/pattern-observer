package com.practicas3.pattern.observer.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class Formulario extends JPanel {

	private static final long serialVersionUID = 1L;

	private JLabel lblApellidos;
	private JLabel lblNombres;
	private JLabel lblTelefono;

	private JTextField txtApellidos;
	private JTextField txtNombres;
	private JTextField txtTelefono;

	public Formulario() {
		super(new GridBagLayout());

		this.lblApellidos = new JLabel("Apellidos:");
		this.lblNombres = new JLabel("Nombres:");
		this.lblTelefono = new JLabel("Tel:");

		this.txtApellidos = new JTextField();
		this.txtApellidos.setColumns(50);
		this.txtNombres = new JTextField(50);
		this.txtTelefono = new JTextField(18);

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridwidth = 1;
		constraints.gridheight = 1;

		// Labels
		constraints.anchor = GridBagConstraints.LINE_END;
		constraints.gridx = 0;
		constraints.gridy = 0;
		this.add(this.lblApellidos, constraints);

		constraints.gridy = 1;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		this.add(this.lblNombres, constraints);

		constraints.gridy = 2;
		this.add(this.lblTelefono, constraints);

		// Fields
		constraints.anchor = GridBagConstraints.LINE_START;
		constraints.gridx = 1;
		constraints.gridy = 0;	
		this.add(this.txtApellidos, constraints);
		
		constraints.gridy = 1;
		this.add(this.txtNombres, constraints);

		constraints.gridy = 2;
		this.add(this.txtTelefono, constraints);
	}

}
