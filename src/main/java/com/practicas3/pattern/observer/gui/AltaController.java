package com.practicas3.pattern.observer.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.event.MouseInputAdapter;

public class AltaController {

	public void dibujarVentanaAlta() {
		
		JFrame frame = new JFrame("Pattern-Observer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.setLayout(new BorderLayout());
		
		Encabezado encabezado = new Encabezado("Titulo", "SubTitulo");
		frame.getContentPane().add(encabezado, BorderLayout.NORTH);

		Formulario formulario = new Formulario();
		JScrollPane jScrollPane = new JScrollPane(formulario);
		frame.getContentPane().add(jScrollPane, BorderLayout.CENTER);
		
		Botonera botonera = new Botonera();
		JScrollPane jScrollPane2 = new JScrollPane(botonera);
		frame.getContentPane().add(jScrollPane2, BorderLayout.SOUTH);
		
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		       
		AltaController sut = new AltaController();
		sut.dibujarVentanaAlta();
	}
}
