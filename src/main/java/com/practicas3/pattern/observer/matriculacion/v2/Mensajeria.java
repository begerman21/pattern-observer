package com.practicas3.pattern.observer.matriculacion.v2;

public class Mensajeria implements Observer {

	public void enviarMensajeAlta(Matricula matricula) {
		
		System.out.println("Enviando mensaje SMS (" + matricula.getCelular() + ") al alumno del alta de su matricula.");
	}

	@Override
	public void notificarme(Matricula matricula) {
		this.enviarMensajeAlta(matricula);
	}
}
