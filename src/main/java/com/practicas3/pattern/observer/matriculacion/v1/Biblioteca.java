package com.practicas3.pattern.observer.matriculacion.v1;

public class Biblioteca {

	public void altaSocio(String socio) {
		
		System.out.println("Se da de alta al nuevo alumno " + socio + " en la Biblioteca,");
	}

}
