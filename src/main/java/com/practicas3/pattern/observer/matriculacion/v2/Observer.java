package com.practicas3.pattern.observer.matriculacion.v2;

public interface Observer {

	public void notificarme(Matricula matricula);
}
