package com.practicas3.pattern.observer.matriculacion.v1;

public class ServicioAlumnado {

	public void nuevoAlumno(String alumno) {
		System.out.println("Se da de alta al nuevo alumno " + alumno + " en Alumnado");
	}

}
