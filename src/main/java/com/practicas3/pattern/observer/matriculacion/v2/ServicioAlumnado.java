package com.practicas3.pattern.observer.matriculacion.v2;

public class ServicioAlumnado implements Observer {

	public void nuevoAlumno(String alumno) {
		System.out.println("Se da de alta al nuevo alumno " + alumno + " en Alumnado");
	}

	@Override
	public void notificarme(Matricula matricula) {
		if (matricula.isNueva()) {
			this.nuevoAlumno(matricula.getAlumno());
		}
	}

}
