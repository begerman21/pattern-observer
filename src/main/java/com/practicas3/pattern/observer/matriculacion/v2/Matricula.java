package com.practicas3.pattern.observer.matriculacion.v2;

import lombok.Data;

@Data
public class Matricula {

	private String numero;
	private String alumno;
	private String curso;
	private String celular;
	private boolean nueva;
	private int edad;
	
}
