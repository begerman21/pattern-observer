package com.practicas3.pattern.observer.matriculacion.v2;

public class Biblioteca implements Observer {

	public void altaSocio(String socio) {

		System.out.println("Se da de alta al nuevo alumno " + socio + " en la Biblioteca,");
	}

	@Override
	public void notificarme(Matricula matricula) {
		if (matricula.isNueva()) {
			this.altaSocio(matricula.getAlumno());
		}
	}

}
