package com.practicas3.pattern.observer.matriculacion.v2;

public class Principal {

	public static void main(String[] args) {
		Matricula matricula = new Matricula();
		matricula.setAlumno("Bam-Bam");
		matricula.setCelular("3786613312");
		matricula.setNueva(true);
		
		ServicioMatriculacion sm = new ServicioMatriculacion();
		
		sm.agregarObserver(new Mensajeria());
		sm.agregarObserver(new ServicioAlumnado());
//		sm.agregarObserver(new Biblioteca());
		
		sm.alta(matricula);
	}
}
