package com.practicas3.pattern.observer.matriculacion.v1;

public class Principal {

	public static void main(String[] args) {
		Matricula matricula = new Matricula();
		matricula.setAlumno("Bam-Bam");
		matricula.setCelular("3786613312");
		matricula.setNueva(true);
		
		new ServicioMatriculacion().alta(matricula);
	}
}
