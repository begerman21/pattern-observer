package com.practicas3.pattern.observer.matriculacion.v1;

public class Mensajeria {

	public void enviarMensajeAlta(Matricula matricula) {
		
		System.out.println("Enviando mensaje SMS (" + matricula.getCelular() + ") al alumno del alta de su matricula.");
	}
}
