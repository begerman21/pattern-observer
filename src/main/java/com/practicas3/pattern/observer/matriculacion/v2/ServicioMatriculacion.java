package com.practicas3.pattern.observer.matriculacion.v2;

import java.util.LinkedList;
import java.util.List;

public class ServicioMatriculacion {
	
	private List<Observer> observers = new LinkedList<>();

	public void alta(Matricula matricula) {

		System.out.println("Dando de alta la nueva matricula : " + matricula);
		
		this.notificar(matricula);
	}

	public void agregarObserver(Observer observer) {
		this.observers.add(observer);
	}

	public void quitarObserver(Observer observer) {
		this.observers.remove(observer);
	}

	public void notificar(Matricula matricula) {
		for(Observer observer: this.observers) {
			observer.notificarme(matricula);
		}
	}

}
