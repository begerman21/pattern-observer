package com.practicas3.pattern.observer.matriculacion.v1;

public class ServicioMatriculacion {

	public void alta(Matricula matricula) {

		System.out.println("Dando de alta la nueva matricula : " + matricula);

		new Mensajeria().enviarMensajeAlta(matricula);

		if (matricula.isNueva()) {
			new ServicioAlumnado().nuevoAlumno(matricula.getAlumno());
			new Biblioteca().altaSocio(matricula.getAlumno());
		}
		
	}

}
