package com.practicas3.pattern.observer.v2;

import org.junit.jupiter.api.Test;

import com.practicas3.pattern.observer.gui.AltaController;

public class ViewTest {

	@Test
	void test() {
		AltaController sut = new AltaController();
		sut.dibujarVentanaAlta();
	}
}
