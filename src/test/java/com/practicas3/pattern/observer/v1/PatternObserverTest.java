package com.practicas3.pattern.observer.v1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import com.practicas3.pattern.observer.cirugia.Cirugia;
import com.practicas3.pattern.observer.cirugia.UseCase;

class PatternObserverTest {

	UseCase sut = new UseCase();

	@Test
	void test() {
		sut.agregarPasiente("Pablo");
		LocalDate hoy = LocalDate.now();
		sut.agregarCirugia("Pablo", hoy, "A corazon abierto");
		sut.agregarCirujano(1, "Pedro");
		sut.agregarFamiliar(1, "Bambam", "Hijo");
		sut.posponerCirugia(1, 7);
		
		Cirugia actual = sut.getCirugia(1);
		
		assertEquals(hoy.plusDays(7), actual.getFecha());
		assertEquals(3, actual.getObservadores().size());
		
		sut.agregarFamiliar(1, "Betty", "Esposa");
		assertEquals(4, actual.getObservadores().size());
	}

}
